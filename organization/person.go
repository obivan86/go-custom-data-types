package organization

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

//type TwitterHandler = string // making alias

type TwitterHandler string //declaring new type

type Identifiable interface {
	ID() string
}

type Citizen interface {
	Identifiable
	Country() string
}

type socialSecurityNumber string

func NewSocialSecurityNumber(value string) Citizen {
	return socialSecurityNumber(value)
}

func (ssn socialSecurityNumber) ID() string {
	return string(ssn)
}

func (ssn socialSecurityNumber) Country() string {
	return "United State of America"
}

type europeanUnionIdentifier struct {
	id      string
	country string
}

func NewEuropeanUnionIdentifier(id interface{}, country string) Citizen {
	switch v := id.(type) {
	case string:
		return europeanUnionIdentifier{
			id:      v,
			country: country,
		}
	case int:
		return europeanUnionIdentifier{
			id:      strconv.Itoa(v),
			country: country,
		}
	default:
		panic("using an invalid type to initialize EU identifier")
	}
}

func (eui europeanUnionIdentifier) ID() string {
	return string(eui.id)
}

func (eui europeanUnionIdentifier) Country() string {
	return eui.country
}

type Name struct {
	first string
	last  string
}

func (tw TwitterHandler) RedirectUrl() string {
	cleanHandler := strings.TrimPrefix(string(tw), "@")
	return fmt.Sprintf("www.twitter.com/%s", cleanHandler)
}

type Person struct {
	Name
	twitterHandler TwitterHandler
	Citizen
}

func NewPerson(firstName, lastName string, citizen Citizen) Person {
	return Person{
		Name: Name{
			first: firstName,
			last:  lastName,
		},
		Citizen: citizen,
	}
}

func (p *Name) FullName() string {
	return fmt.Sprintf("%s %s", p.first, p.last)
}

func (p *Person) SetTwitterHandler(handler TwitterHandler) error {
	if len(handler) == 0 {
		p.twitterHandler = handler
	} else if !strings.HasPrefix(string(handler), "@") {
		return errors.New("twitter handler must start with an @ symbol")
	}

	p.twitterHandler = handler
	return nil
}

func (p *Person) TwitterHandler() TwitterHandler {
	return p.twitterHandler
}

func (p *Person) ID() string {
	return p.Citizen.ID()
}
