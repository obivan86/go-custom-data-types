package main

import (
	"fmt"
	"obivan.org/obivan/go-custom-data-types/organization"
)

func main() {
	p := organization.NewPerson("Ivan", "Obradovic", organization.NewSocialSecurityNumber("12-34-78"))
	err := p.SetTwitterHandler(organization.TwitterHandler("@ivan_obradovic"))
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(p.TwitterHandler())
	fmt.Println(p.TwitterHandler().RedirectUrl())
	fmt.Println(p.FullName())
	fmt.Println(p.ID())
	fmt.Println(p.Country())
	fmt.Println("-------------------------------")

	newPerson := organization.NewPerson("Eu Ivan", "Obradovic", organization.NewEuropeanUnionIdentifier("12-34-56", "Serbia"))
	err = newPerson.SetTwitterHandler(organization.TwitterHandler("@eu_ivan_obradovic"))
	if err != nil {
		fmt.Println(err.Error())
	}
	fmt.Println(newPerson.TwitterHandler())
	fmt.Println(newPerson.TwitterHandler().RedirectUrl())
	fmt.Println(newPerson.FullName())
	fmt.Println(newPerson.Country())
	fmt.Println(newPerson.ID())
}
